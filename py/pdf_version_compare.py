import re
from PyPDF2.pdf import PdfFileReader


def version_compare(input_file, expected_version):
    try:
        file = open(input_file, 'rb')
        doc = PdfFileReader(file)
        doc.stream.seek(0) # Necessary since the comment is ignored for the PDF analysis
        val = "".join(chr(x) for x in doc.stream.readline())
        version = re.findall("\d+\.\d+", val)
        if(float(version.__getitem__(0)) == expected_version):
            return True;
        else:
            return False;
    except Exception as e:
        raise Exception("9005")
