import sys
import boto3
import os
import rest
import json

input = sys.argv[1]
output = sys.argv[2]
apiType = sys.argv[3]
authKey = sys.argv[4]

if os.getenv("AWS_ACCESS_KEY_ID") or os.getenv("AWS_SECRET_ACCESS_KEY") or os.getenv("AWS_REGION") is None:
    os.environ["AWS_ACCESS_KEY_ID"] = "AKIATYMWK536TWG6UNB7"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "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"
    os.environ["AWS_REGION"] = "ap-south-1"
s3client = boto3.client('s3', region_name=os.environ["AWS_REGION"])

input_url = s3client.generate_presigned_url(
    'get_object', {'Bucket': 'nextgenflow-qa', 'Key': input})

print('Generated Presigned Input URL to take from: '+input_url)

output_url = s3client.generate_presigned_url(
    'put_object', {'Bucket': 'nextgenflow-qa', 'Key': output})


print('Generated Presigned Output URL to put into: '+output_url)
if apiType=='cutout':
    response = rest.adobe_uri_sensei(apiType,input_url,output_url,authKey)
else:
    response = rest.adobe_uri(apiType,input_url,output_url,authKey)

if response.status_code==200 or response.status_code==202:
    print('Response Retrived'+json.dumps(response.json()))
    response_url = s3client.generate_presigned_url(
        'get_object', {'Bucket': 'nextgenflow-qa', 'Key': output})
    print(response_url)

    responseData = '{ "data" : "'+response_url+'"}'
    fileName = 'response.json'
    file = open(fileName, 'w')
    file.write(responseData)

else:
    print('Response Retrived' + json.dumps(response.json()))
    fileName = 'response.json'
    file = open(fileName, 'w')
    file.write(json.dumps(response.json()))
    raise Exception("Error while getting data from url. Please check response.json for more details")