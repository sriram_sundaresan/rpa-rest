import json_parse as json_parse
import pdf_version_worker_thread as pdfThread
import sys
import json

variable_name = sys.argv[1]
corp_id = sys.argv[2]
keys = sys.argv[3]
input_file = sys.argv[4]
general_corp_id = sys.argv[5]
ip_address = sys.argv[6]
workflow_ip = sys.argv[7]
if workflow_ip == '172.16.12.12':
    server = 'Y:/'
else:
    server = 'Z:/'
status_file = open('response.json', 'w')
try:
    key_list = keys.split(',')
    for key in key_list:
        if 'pdfVersion' in key:
            pdf_version = json_parse.__parse_and_get(variable_name,key,general_corp_id)
        elif 'pdfFileSize' in key:
            pdf_size = json_parse.__parse_and_get(variable_name, key, general_corp_id)
    #download_status = filedownload.download_file_from_samba_server(ip_address+"/"+input_file)
    input_files = input_file.split(',')
    result_list = list()
    thread_list = list()
    for i in input_files:
        i = server+ip_address+"/"+i
        thread = pdfThread.PDFWorker(i, pdf_version, pdf_size, result=None)
        thread_list.append(thread)

    for thread in thread_list:
        thread.start()
        result_list.append(thread.join())

    print(result_list)
    response_json_str = '{"response": { "pdf_version": { "success": [], "failed": [] }, "pdf_size": { "success": [], ' \
                        '	"failed": []} }} '
    response_json = json.loads(response_json_str)
    for result_dict in result_list:
        if result_dict['size_code'] == '10000':
            response_json['response']['pdf_size']['success'].append(result_dict['file_name'])
        else:
            failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['size_code']}
            response_json['response']['pdf_size']['failed'].append(failed_response_dict)

        if result_dict['version_code'] == '10000':
            response_json['response']['pdf_version']['success'].append(result_dict['file_name'])
        else:
            failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['version_code']}
            response_json['response']['pdf_version']['failed'].append(failed_response_dict)

    status_file.write(json.dumps(response_json))

except Exception as e:
    status_file.write('{"response" : "Failed", "reason" : "'+str(e)+'"}')