import boto3
import os
import requests
from smb.SMBConnection import SMBConnection


def download_file(input):
    s3client = boto3.client('s3', region_name=os.environ['AWS_REGION'])
    bucket_name = os.environ['bucket_name']
    input_url = s3client.generate_presigned_url(
        'get_object', {'Bucket': bucket_name, 'Key': input})
    r = requests.get(url=input_url)

    os.makedirs(os.path.dirname(input), exist_ok=True)
    with open(input, "wb") as f:
        f.write(r.content)
    if(os.path.exists(input)):
        return True;
    return False;


def download_file_from_samba_server(input):
    if os.getenv('WF_USER') is not None and os.getenv('WF_PWD') is not None and os.getenv('WF_IP') is not None:
        conn = SMBConnection(os.environ['WF_USER'], os.environ['WF_PWD'], os.environ['WF_IP'], 'WF', use_ntlm_v2=True)
    else:
        raise Exception('General Bot Error: LAN Credentials are not valid')
    try:
        assert conn.connect(os.environ['WF_IP'], 139)
        local_file = os.path.basename(input)
        with open(local_file, 'wb') as fp:
            conn.retrieveFile('userworkspace', input, fp)
        if(os.path.exists(local_file)):
            return True;
        return False;
    except:
        raise Exception('Path not valid '+input)
