from threading import Thread
import pdf_version_compare as pdfCompare
import os


class PDFWorker(Thread):

    def __init__(self, file_name, expected_version, expected_size, result):
        Thread.__init__(self)
        self.file_name = file_name
        self.expected_version = expected_version
        self.expected_size = expected_size
        self.result = result
        self._return = None

    def run(self):
        print("Running Thread with file: " + self.file_name)
        try:
            result = pdfCompare.version_compare(self.file_name, self.expected_version)
            size = os.path.getsize(filename=self.file_name)
            if size <= int(self.expected_size):
                size_code = '10000'
            else:
                size_code = '10013'

            if result is True:
                version_code = '10000'
            else:
                version_code = '10011'

            self.result = {'file_name':self.file_name, 'size_code': size_code, 'version_code':version_code}
        except Exception as e:
            print(str(e))
            print("File is not present on the provided location or path " + self.file_name)
            self.result = {'file_name':self.file_name, 'version_code': '10012', 'size_code': '10012'}
            # self.result = 'FAILURE: File is not present on the provided location or path: '+self.file_name;
        # self._return = self._target(*self._args, **self._kwargs)

    def join(self):
        Thread.join(self)
        return self.result
