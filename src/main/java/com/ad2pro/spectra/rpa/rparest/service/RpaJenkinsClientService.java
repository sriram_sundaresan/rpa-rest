package com.ad2pro.spectra.rpa.rparest.service;

import com.ad2pro.spectra.rpa.rparest.config.properties.JenkinsProperties;
import com.google.common.io.CharStreams;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Artifact;
import com.offbytwo.jenkins.model.BuildWithDetails;
import com.offbytwo.jenkins.model.JobWithDetails;
import com.offbytwo.jenkins.model.QueueReference;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHeaders;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@Service
@Slf4j
@Data
public class RpaJenkinsClientService {

    public static JenkinsServer jenkinsServer;

    @Autowired
    private JenkinsProperties jenkinsProperties;

    @EventListener(ApplicationReadyEvent.class)
    public void init() throws URISyntaxException {
        jenkinsServer = new JenkinsServer(new URI(jenkinsProperties.getUrl()), jenkinsProperties.getUsername(),
                jenkinsProperties.getToken());
    }

    public String trigger(String jobName, Map<String, String> params, String requestId) throws IOException {
        /*synchronized (jobName) {*/
            QueueReference queueReference = jenkinsServer.getJob(jobName).build(params,true);
            log.info("queue_no is {}",queueReference.getQueueItemUrlPart());
            return queueReference.getQueueItemUrlPart();
        /*}*/
    }

    public JSONObject getArtifactsFor(String bot_name, int deployment_no) {
        JSONObject result = new JSONObject();
        try {
            JobWithDetails job = jenkinsServer.getJob(bot_name);
            BuildWithDetails buildWithDetails = job.getBuildByNumber(deployment_no).details();
            Optional<Artifact> optionalArtifact = buildWithDetails.getArtifacts().stream().
                    filter(artifact -> artifact.getFileName().equalsIgnoreCase("response.json"))
                    .findFirst();
            if(optionalArtifact.isPresent()) {
                try {
                    InputStream is = buildWithDetails.downloadArtifact(optionalArtifact.get());
                    InputStreamReader reader = new InputStreamReader(is);
                    String response = CharStreams.toString(reader);

                    result = new JSONObject(response);

                } catch (URISyntaxException e) {
                    result.put("output","unable to download artifact");
                }
            } else {
                result.put("output","{}");
            }

        } catch (IOException e) {
            log.error("Error while getting job",e);
        }
        return result;
    }
}
