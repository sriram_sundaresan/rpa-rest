package com.ad2pro.spectra.rpa.rparest.utils;

import com.ad2pro.spectra.rpa.rparest.service.RpaJenkinsClientService;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class ParallelWorkerThread implements Runnable {

    private String botName;
    private RpaJenkinsClientService rpaJenkinsClientService;

    public ParallelWorkerThread(RpaJenkinsClientService rpaJenkinsClientService, String botName) {
        this.rpaJenkinsClientService = rpaJenkinsClientService;
        this.botName = botName;
    }

    @Override
    public void run() {
        Map<String,String> requestParams = new HashMap<>();
        String requestId = UUID.randomUUID().toString();
        requestParams.put("scriptName", "/tmp/jdrpa/rest.py");
        requestParams.put("requestId",requestId);
        requestParams.put("token","welcome");
        try {
            log.info("Executing thread with request ID {}",requestId);
            String queue = rpaJenkinsClientService.trigger(botName, requestParams,requestId);
        } catch (IOException e) {
            log.error("Error while triggering job ",e);
        }
    }
}
