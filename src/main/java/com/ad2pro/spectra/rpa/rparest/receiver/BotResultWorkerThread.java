package com.ad2pro.spectra.rpa.rparest.receiver;

import com.ad2pro.spectra.rpa.rparest.builder.BotBuilder;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

public class BotResultWorkerThread implements Runnable {

    private BotBuilder botBuilder;
    private Map<String,Object> event;

    public BotResultWorkerThread(BotBuilder botBuilder, Map<String,Object> event) {
        this.botBuilder = botBuilder;
        this.event = event;
    }

    @Override
    public void run() {
        botBuilder.notifyBuildCompletionAndUnlock(new JSONObject(event));
    }
}
