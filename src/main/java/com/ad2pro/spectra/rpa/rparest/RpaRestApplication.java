package com.ad2pro.spectra.rpa.rparest;

import com.ad2pro.spectra.common.redisclient.RedisClientApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.ad2pro.spectra"})
@EntityScan("com.ad2pro.spectra")
@EnableJpaRepositories("com.ad2pro.spectra")
@Import( {RedisClientApplication.class})

public class RpaRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RpaRestApplication.class, args);
	}

}
