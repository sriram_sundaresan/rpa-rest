package com.ad2pro.spectra.rpa.rparest.utils;

import org.jenkinsci.plugins.pipeline.modeldefinition.model.Agent;
import org.jenkinsci.plugins.pipeline.modeldefinition.parser.PipelineStepFinder;
import org.springframework.stereotype.Component;

@Component
public class FlowDefinitionGeneratorUtils {
    public String getBasicScript() {
        return "node('<%nodeName%>') {" + System.lineSeparator() + "<%script%>}";
    }

    public String generatePipelineScript() {
        return "pipeline {" + System.lineSeparator()
                + "agent { node { label '<%labelExpression%>' } }" + System.lineSeparator()
                + "stages { " + System.lineSeparator()
                + "stage('Build') {" + System.lineSeparator()
                + "steps {<%stepExpression%>" + System.lineSeparator()
                + "}" + System.lineSeparator()
                + "}" + System.lineSeparator()
                + "}" + System.lineSeparator()
                +"<%postBuildExpression%>" + System.lineSeparator()
                + "}" + System.lineSeparator();
    }

    public String generatePostBuildExpression() {
        return "post {" + System.lineSeparator()
                +"always {" + System.lineSeparator()
                +"<%postBuildScripts%>" + System.lineSeparator()
                +" }" + System.lineSeparator()
                +" }" + System.lineSeparator();
    }
}
