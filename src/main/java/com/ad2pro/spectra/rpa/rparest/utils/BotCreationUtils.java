package com.ad2pro.spectra.rpa.rparest.utils;
import com.ad2pro.spectra.rpa.rparest.domain.BotScriptLocation;
import com.ad2pro.spectra.rpa.rparest.domain.LocationType;
import com.ad2pro.spectra.rpa.rparest.domain.PythonBot;
import com.thoughtworks.xstream.XStream;
import com.tikal.hudson.plugins.notification.*;
import hudson.ExtensionList;
import hudson.XmlFile;
import hudson.model.*;
import hudson.plugins.git.GitSCM;
import hudson.slaves.ComputerLauncher;
import hudson.slaves.DumbSlave;
import hudson.tasks.ArtifactArchiver;
import hudson.triggers.Trigger;
import hudson.util.XStream2;
import jenkins.authentication.tokens.api.AuthenticationTokenSource;
import jenkins.authentication.tokens.api.AuthenticationTokens;
import jenkins.model.Jenkins;
import jenkins.plugins.git.GitStep;
import jenkins.plugins.logstash.LogstashNotifier;
import jenkins.plugins.logstash.pipeline.LogstashSendStep;
import lombok.extern.slf4j.Slf4j;
import org.jenkinsci.plugins.gitclient.Git;
import org.jenkinsci.plugins.pipeline.modeldefinition.ast.ModelASTPipelineDef;
import org.jenkinsci.plugins.pipeline.modeldefinition.parser.Converter;
import org.jenkinsci.plugins.pipeline.modeldefinition.parser.JSONParser;
import org.jenkinsci.plugins.structs.describable.DescribableModel;
import org.jenkinsci.plugins.structs.describable.DescribableParameter;
import org.jenkinsci.plugins.structs.describable.UninstantiatedDescribable;
import org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition;
import org.jenkinsci.plugins.workflow.cps.Snippetizer;
import org.jenkinsci.plugins.workflow.flow.FlowDefinition;
import org.jenkinsci.plugins.workflow.job.WorkflowJob;
import org.jenkinsci.plugins.workflow.job.WorkflowJobProperty;
import org.jenkinsci.plugins.workflow.job.WorkflowRun;
import org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty;
import org.jenkinsci.plugins.workflow.steps.ArtifactArchiverStep;
import org.jenkinsci.plugins.workflow.steps.Step;
import org.jenkinsci.plugins.workflow.steps.StepDescriptor;
import org.jenkinsci.plugins.workflow.steps.durable_task.BatchScriptStep;
import org.junit.Rule;
import org.jvnet.hudson.test.JenkinsRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

@Slf4j
@Component
public class BotCreationUtils {

    @Autowired
    FlowDefinitionGeneratorUtils flowDefinitionGeneratorUtils;

    public String createBot(PythonBot bot) {
        String jobName = bot.getBotName();
        String flowDefinitionScript = flowDefinitionGeneratorUtils.generatePipelineScript();
        flowDefinitionScript = flowDefinitionScript.replace("<%labelExpression%>",bot.getNode());
        BotScriptLocation scriptLocation = bot.getScriptLocation();
        String subScripts = "";

        //If the scripts are to be taken from SCM (Git), based on scm parameters git chedkou
        if(LocationType.SCM.equals(scriptLocation.getLocationType())) {
            Map<String,Object> scmVariableMap = new HashMap<>();
            scmVariableMap.put("url",scriptLocation.getScmUrl());
            scmVariableMap.put("credentialsId",scriptLocation.getCredential());
            subScripts = generatePipelineScript("git",GitStep.class,scmVariableMap) + System.lineSeparator();
        }

        Map<String, Object> batchscriptVariableMap = new HashMap<>();
        batchscriptVariableMap.put("script", "python "+bot.getScriptName());
        batchscriptVariableMap.put("returnStdout",false);
        batchscriptVariableMap.put("encoding", "");
        batchscriptVariableMap.put("returnStatus", false);
        batchscriptVariableMap.put("label", "execution");

        subScripts = subScripts + generatePipelineScript("bat",BatchScriptStep.class,batchscriptVariableMap) + System.lineSeparator();

        Map<String,Object> archiveMap = new HashMap<>();
        archiveMap.put("artifacts","response.json");
        archiveMap.put("excludes", "");
        archiveMap.put("allowEmptyArchive",false);
        archiveMap.put("onlyIfSuccessful",true);
        archiveMap.put("fingerprint", false);
        archiveMap.put("defaultExcludes", true);
        archiveMap.put("caseSensitive", true);

        subScripts = subScripts + generatePipelineScript("archiveArtifacts",ArtifactArchiverStep.class,archiveMap)+  System.lineSeparator();
        flowDefinitionScript = flowDefinitionScript.replace("<%stepExpression%>",subScripts);

        Map<String, Object> logStashMap = new HashMap<>();
        logStashMap.put("failBuild",false);
        logStashMap.put("maxLines",0);

        String logStashScript = generatePipelineScript("logstashSend",LogstashSendStep.class,logStashMap);

        String postBuildExpression = "";
        postBuildExpression = postBuildExpression + flowDefinitionGeneratorUtils.generatePostBuildExpression();
        postBuildExpression = postBuildExpression.replace("<%postBuildScripts%>", logStashScript);
        flowDefinitionScript = flowDefinitionScript.replace("<%postBuildExpression%>",postBuildExpression);

        WorkflowJob workflowJob = new WorkflowJob(Jenkins.getInstance(),jobName);

        List<ParameterDefinition> parameterDefinitionList = new ArrayList();
        for(Map.Entry entry : bot.getBotParams().entrySet()) {
            StringParameterDefinition stringParameterDefinition = new StringParameterDefinition(entry.getKey().toString(),entry.getValue().toString());
            parameterDefinitionList.add(stringParameterDefinition);
        }

        ParametersDefinitionProperty parametersDefinitionProperty = new ParametersDefinitionProperty(parameterDefinitionList);
        try {
            workflowJob.addProperty(parametersDefinitionProperty);
        }
        catch(IllegalStateException e) {
            log.error("Ignoring Error on parent");
        }
        catch (IOException e) {
            log.error("Error while setting parameters",e);
        }

        /*List<Endpoint> endpoints = new ArrayList<>();
        UrlInfo urlInfo = new UrlInfo(UrlType.PUBLIC,"http://localhost:8087/rpa/post");
        Endpoint endpoint = new Endpoint(urlInfo);
        endpoint.setEvent("completed");
        endpoint.setTimeout(30000);
        endpoint.setLoglines(0);
        endpoint.setRetries(0);
        endpoint.setProtocol(Protocol.HTTP);
        endpoint.setFormat(Format.JSON);
        endpoints.add(endpoint);
        HudsonNotificationProperty hudsonNotificationProperty =new HudsonNotificationProperty(endpoints);



        try {
            workflowJob.addProperty(hudsonNotificationProperty);
        }
        catch(IllegalStateException e) {
            log.error("Ignoring Error on parent");
        }
        catch (IOException e) {
            log.error("Error while setting parameters",e);
        }*/

        CpsFlowDefinition cpsFlowDefinition =
                new CpsFlowDefinition(flowDefinitionScript, true);
        try {
            workflowJob.setDefinition(cpsFlowDefinition);
        }
        catch (IllegalStateException ex) {
            log.error("Ignoring Error on parent");
        }

        XStream2 xStream = new XStream2();
        String xmlAsString = xStream.toXML(workflowJob);
        log.info(xmlAsString);
        return xmlAsString;
    }

    public String generatePipelineScript(String symbol, Class clazz, Map<String,Object> parameters) {
        UninstantiatedDescribable uninstantiatedDescribable =
                new UninstantiatedDescribable(symbol,clazz.getName(),parameters);

        StringBuilder value = SnippetGeneratorUtils.ud2groovy(new StringBuilder(),uninstantiatedDescribable,false,false);
        return value.toString();
    }
}
