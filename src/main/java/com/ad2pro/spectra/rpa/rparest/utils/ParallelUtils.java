package com.ad2pro.spectra.rpa.rparest.utils;

import com.ad2pro.spectra.rpa.rparest.service.RpaJenkinsClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class ParallelUtils implements Runnable {
    private RpaJenkinsClientService rpaJenkinsClientService;
    private String botName;

    public ParallelUtils(RpaJenkinsClientService rpaJenkinsClientService, String botName) {
        this.rpaJenkinsClientService = rpaJenkinsClientService;
        this.botName = botName;
    }

    @Override
    public void run() {
        for(int i=0; i<1500;i++) {
            Thread t = new Thread(new ParallelWorkerThread(rpaJenkinsClientService,botName));
            t.start();
        }
    }
}
