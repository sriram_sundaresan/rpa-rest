package com.ad2pro.spectra.rpa.rparest.domain;

public enum LocationType {
    LOCAL,
    FTP,
    SCM
}
