package com.ad2pro.spectra.rpa.rparest.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "pythonBot", value = PythonBot.class),
})
public class BasicBot {
    @JsonProperty("botName")
    protected String botName;
    @JsonProperty("botParams")
    protected Map<String,Object> botParams;
    @JsonProperty("node")
    protected String node;
}
