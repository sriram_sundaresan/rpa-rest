package com.ad2pro.spectra.rpa.rparest.dao;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "bot_results")
@Data
@NoArgsConstructor
public class BotResults {

    @Id
    @Column(name = "request_id")
    private String requestId;

    @NotNull
    @Column(unique = false, name = "result")
    private String result;
}
