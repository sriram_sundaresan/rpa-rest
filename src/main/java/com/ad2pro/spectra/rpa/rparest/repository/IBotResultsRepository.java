package com.ad2pro.spectra.rpa.rparest.repository;

import com.ad2pro.spectra.rpa.rparest.dao.BotResults;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBotResultsRepository extends CrudRepository<BotResults,String> {
}
