package com.ad2pro.spectra.rpa.rparest.utils;

import lombok.extern.slf4j.Slf4j;
import org.jenkinsci.plugins.structs.describable.DescribableModel;
import org.jenkinsci.plugins.structs.describable.DescribableParameter;
import org.jenkinsci.plugins.structs.describable.UninstantiatedDescribable;
import org.jenkinsci.plugins.workflow.steps.Step;
import org.jenkinsci.plugins.workflow.steps.StepDescriptor;

import javax.lang.model.SourceVersion;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

@Slf4j
public class SnippetGeneratorUtils {
    public static String step2Groovy(Step s) throws UnsupportedOperationException {
        return object2Groovy(new StringBuilder(), s, false).toString();
    }

    public static StringBuilder object2Groovy(StringBuilder b, Object o, boolean nestedExp) throws UnsupportedOperationException {
        if (o == null) {
            return b.append("null");
        } else {
            Class<?> clazz = o.getClass();
            if (clazz != String.class && clazz != Character.class) {
                if (clazz != Boolean.class && clazz != Integer.class && clazz != Long.class && clazz != Float.class && clazz != Double.class && clazz != Byte.class && clazz != Short.class) {
                    if (o instanceof List) {
                        return list2groovy(b, (List)o);
                    } else if (o instanceof Map) {
                        return map2groovy(b, (Map)o);
                    } else if (o instanceof UninstantiatedDescribable) {
                        return ud2groovy(b, (UninstantiatedDescribable)o, false, nestedExp);
                    } else {
                        Iterator var17 = StepDescriptor.all().iterator();

                        StepDescriptor d;
                        do {
                            if (!var17.hasNext()) {
                                return b.append("<object of type ").append(clazz.getCanonicalName()).append('>');
                            }

                            d = (StepDescriptor)var17.next();
                        } while(!d.clazz.equals(clazz));

                        Step step = (Step)o;
                        UninstantiatedDescribable uninst = d.uninstantiate(step);
                        boolean blockArgument = d.takesImplicitBlockArgument();
                        if (d.isMetaStep()) {
                            DescribableModel<?> m = new DescribableModel(d.clazz);
                            DescribableParameter p = m.getFirstRequiredParameter();
                            if (p != null) {
                                Object wrapped = uninst.getArguments().get(p.getName());
                                if (wrapped instanceof UninstantiatedDescribable) {
                                    boolean failSimplification = false;
                                    UninstantiatedDescribable nested = (UninstantiatedDescribable)wrapped;
                                    TreeMap<String, Object> copy = new TreeMap(nested.getArguments());
                                    Iterator var15 = uninst.getArguments().entrySet().iterator();

                                    while(var15.hasNext()) {
                                        Map.Entry<String, ?> e = (Map.Entry)var15.next();
                                        if (!((String)e.getKey()).equals(p.getName()) && copy.put(e.getKey(), e.getValue()) != null) {
                                            failSimplification = true;
                                        }
                                    }

                                    if (!canUseMetaStep(nested)) {
                                        failSimplification = true;
                                    }

                                    if (!failSimplification) {
                                        UninstantiatedDescribable combined = new UninstantiatedDescribable(nested.getSymbol(), nested.getKlass(), copy);
                                        combined.setModel(nested.getModel());
                                        return ud2groovy(b, combined, blockArgument, nestedExp);
                                    }
                                }
                            } else {
                                log.info("Buggy meta-step " + d.clazz + " defines no mandatory parameter");
                            }
                        }

                        uninst.setSymbol(d.getFunctionName());
                        return functionCall(b, uninst, blockArgument, nestedExp);
                    }
                } else {
                    return b.append(o);
                }
            } else {
                String text = String.valueOf(o);
                if (text.contains("\n")) {
                    b.append("'''").append(text.replace("\\", "\\\\").replace("'", "\\'")).append("'''");
                } else {
                    b.append('\'').append(text.replace("\\", "\\\\").replace("'", "\\'")).append('\'');
                }

                return b;
            }
        }
    }

    public static boolean canUseMetaStep(UninstantiatedDescribable ud) {
        return canUseSymbol(ud) && StepDescriptor.metaStepsOf(ud.getSymbol()).size() == 1;
    }

    public static StringBuilder list2groovy(StringBuilder b, List<?> o) {
        b.append('[');
        boolean first = true;

        Object elt;
        for(Iterator var3 = o.iterator(); var3.hasNext(); object2Groovy(b, elt, true)) {
            elt = var3.next();
            if (first) {
                first = false;
            } else {
                b.append(", ");
            }
        }

        return b.append(']');
    }

    public static StringBuilder map2groovy(StringBuilder b, Map<?, ?> map) {
        b.append('[');
        mapWithoutBracket2groovy(b, map);
        return b.append(']');
    }

    public static void mapWithoutBracket2groovy(StringBuilder b, Map<?, ?> map) {
        boolean first = true;
        Iterator var3 = map.entrySet().iterator();

        while(var3.hasNext()) {
            Map.Entry<?, ?> entry = (Map.Entry)var3.next();
            if (first) {
                first = false;
            } else {
                b.append(", ");
            }

            Object key = entry.getKey();
            if (key instanceof String && SourceVersion.isName((String)key)) {
                b.append(key);
            } else {
                object2Groovy(b, key, true);
            }

            b.append(": ");
            object2Groovy(b, entry.getValue(), true);
        }

    }

    public static StringBuilder ud2groovy(StringBuilder b, UninstantiatedDescribable ud, boolean blockArgument, boolean nested) {
        return !canUseSymbol(ud) ? map2groovy(b, ud.toShallowMap()) : functionCall(b, ud, blockArgument, nested);
    }

    public static boolean canUseSymbol(UninstantiatedDescribable ud) {
        if (ud.getSymbol() == null) {
            return false;
        } else {
            return StepDescriptor.byFunctionName(ud.getSymbol()) == null;
        }
    }

    public static StringBuilder functionCall(StringBuilder b, UninstantiatedDescribable ud, boolean blockArgument, boolean nested) {
        Map<String, ?> args = ud.getArguments();
        boolean needParenthesis = blockArgument ^ args.isEmpty() || isSingleMap(args) || isSingleList(args) || nested;
        b.append(ud.getSymbol());
        b.append((char)(needParenthesis ? '(' : ' '));
        if (ud.hasSoleRequiredArgument()) {
            object2Groovy(b, args.values().iterator().next(), true);
        } else {
            mapWithoutBracket2groovy(b, args);
        }

        if (needParenthesis) {
            b.append(')');
        }

        if (blockArgument) {
            if (!args.isEmpty()) {
                b.append(' ');
            }

            b.append("{\n    // some block\n}");
        }

        return b;
    }

    public static boolean isSingleMap(Map<String, ?> args) {
        if (args.size() != 1) {
            return false;
        } else {
            Object v = args.values().iterator().next();
            if (v instanceof Map) {
                return true;
            } else if (v instanceof UninstantiatedDescribable) {
                return !canUseSymbol((UninstantiatedDescribable)v);
            } else {
                return false;
            }
        }
    }

    public static boolean isSingleList(Map<String, ?> args) {
        if (args.size() != 1) {
            return false;
        } else {
            Object v = args.values().iterator().next();
            return v instanceof List;
        }
    }

    public String getUrlName() {
        return "pipeline-syntax";
    }

    public String getIconFileName() {
        return null;
    }

    public String getDisplayName() {
        return null;
    }
}
