package com.ad2pro.spectra.rpa.rparest.utils;

import com.thoughtworks.xstream.XStream;
import hudson.XmlFile;
import hudson.util.XStream2;

import java.io.File;

public class XmlFileUtils {
    public static final XStream XSTREAM = new XStream2();

    public static XmlFile createXmlFile() {
        return new XmlFile(XSTREAM, new File("E:\\root","config.xml"));
    }
}
