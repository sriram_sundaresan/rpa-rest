package com.ad2pro.spectra.rpa.rparest.builder;

import com.ad2pro.spectra.common.config.RedisConfiguration;
import com.ad2pro.spectra.common.consumer.CommonConsumer;
import com.ad2pro.spectra.common.exception.MessageConsumerException;
import com.ad2pro.spectra.common.exception.MessageProducerException;
import com.ad2pro.spectra.common.producer.CommonProducer;
import com.ad2pro.spectra.rpa.rparest.dao.BotResults;
import com.ad2pro.spectra.rpa.rparest.repository.IBotResultsRepository;
import com.ad2pro.spectra.rpa.rparest.service.RpaJenkinsClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.CharStreams;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.redisson.api.RCountDownLatch;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Slf4j
@Data
@Component
public class BotBuilder {
    @Autowired
    private RpaJenkinsClientService rpaJenkinsClientService;

    @Autowired
    private IBotResultsRepository botResultsRepository;

    //private BlockingQueue<JSONObject> buildResult = new ArrayBlockingQueue<>(1);
    private JSONObject buildStatus;

    @Autowired
    private RedisConfiguration redisConfiguration;

    private RedissonClient redissonClient;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private RpaJenkinsClientService jenkinsClientService;

    private RedissonClient getRedisClient() {
        if(null==redissonClient) {
            redissonClient = redisConfiguration.getRedisClient();
        }
        return redissonClient;
    }

    public void notifyBuildCompletionAndUnlock(JSONObject jsonObject) {

        Object requestId = jsonObject.getJSONObject("data").getJSONObject("buildVariables").get("requestId");
        /*JSONObject botOutput = rpaJenkinsClientService.
                getArtifactsFor(jsonObject.getString("name"), jsonObject.getJSONObject("build").getInt("number"));
        jsonObject.getJSONObject("build").put("botOutput", botOutput);*/
        BotResults botResults = new BotResults();
        botResults.setRequestId(requestId.toString());
        botResults.setResult(jsonObject.toString());
        botResultsRepository.save(botResults);
        this.buildStatus = jsonObject;
        RCountDownLatch countDownLatch = getRedisClient().getCountDownLatch(requestId.toString());
        countDownLatch.countDown();
    }

    public Map<String,Object> triggerJobSync(String botName, Map<String,String> requestParams, String requestId) throws Exception {
        try {
            rpaJenkinsClientService.trigger(botName,requestParams,requestId);
        } catch (IOException e) {
            throw new Exception("Error while building the job",e);
        }

        RCountDownLatch countDownLatch = getRedisClient().getCountDownLatch(requestId);
        countDownLatch.trySetCount(1);
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            log.error("Error while waiting for Job completion",e);
        }
        BotResults botResults = botResultsRepository.findById(requestId).get();
        JSONObject jsonObject = objectMapper.convertValue(botResults.getResult(),JSONObject.class);
        return jsonObject.toMap();
    }
}
