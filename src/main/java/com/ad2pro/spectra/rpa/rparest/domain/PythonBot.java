package com.ad2pro.spectra.rpa.rparest.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PythonBot extends BasicBot {
    @JsonProperty("scriptName")
    private String scriptName;
    @JsonProperty("scriptLocation")
    private BotScriptLocation scriptLocation;
}
