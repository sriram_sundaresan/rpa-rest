package com.ad2pro.spectra.rpa.rparest.controller;

import com.ad2pro.spectra.common.config.RedisConfiguration;
import com.ad2pro.spectra.rpa.rparest.domain.BasicBot;
import com.ad2pro.spectra.rpa.rparest.domain.PythonBot;
import com.ad2pro.spectra.rpa.rparest.utils.BotCreationUtils;
import com.ad2pro.spectra.rpa.rparest.builder.BotBuilder;
import com.ad2pro.spectra.rpa.rparest.service.RpaJenkinsClientService;
import com.ad2pro.spectra.rpa.rparest.utils.ParallelUtils;
import com.google.common.io.CharStreams;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.client.JenkinsHttpClient;
import com.offbytwo.jenkins.model.*;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.*;

@RestController
@Slf4j
public class RpaJenkinsClientController {
    @Autowired
    RpaJenkinsClientService rpaJenkinsClientService;

    @Autowired
    BotCreationUtils botCreationUtils;

    @Autowired
    BotBuilder botBuilder;

    int i=0;

    @RequestMapping(method = RequestMethod.GET, value = "/rpa/build/response", produces = "application/json")
    public Map<String,Object> getBuildResponseFor(@RequestParam("bot_name") String bot_name, @RequestParam("deployment_no") int deployment_no) {
        Map<String,Object> result = new HashMap<>();
        JenkinsServer jenkinsServer = RpaJenkinsClientService.jenkinsServer;
        try {
            JobWithDetails job = jenkinsServer.getJob(bot_name);
            BuildWithDetails buildWithDetails = job.getBuildByNumber(deployment_no).details();
            Optional<Artifact> optionalArtifact = buildWithDetails.getArtifacts().stream().
                    filter(artifact -> artifact.getFileName().equalsIgnoreCase("response.json"))
                    .findFirst();
            result.put("RESULT",buildWithDetails.getResult().name());
            if(optionalArtifact.isPresent()) {
                try {
                    InputStream is = buildWithDetails.downloadArtifact(optionalArtifact.get());
                    InputStreamReader reader = new InputStreamReader(is);
                    String response = CharStreams.toString(reader);

                    JSONObject jsonObject = new JSONObject(response);
                    result.put("response",jsonObject.toMap());
                } catch (URISyntaxException e) {
                    result.put("output","unable to download artifact");
                }
            } else {
                result.put("output","{}");
            }

        } catch (IOException e) {
            log.error("Error while getting job",e);
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, value="/rpa/dashboard/status", produces=MediaType.APPLICATION_JSON_VALUE)
    public View getDashboardStatus() {
       log.info("");
       JenkinsServer jenkinsServer = RpaJenkinsClientService.jenkinsServer;
        try {
            View view = jenkinsServer.getView("All");
            return view;
        } catch (IOException e) {
            log.error("Error fetching View Details",e);
            return null;
        }
    }


    @RequestMapping(method = RequestMethod.POST, value="/rpa/post",  consumes =MediaType.APPLICATION_JSON_VALUE ,produces=MediaType.TEXT_PLAIN_VALUE)
    public String notifyBuildCompletion(@RequestBody Map<String,Object> jsonObject) {
        log.info(jsonObject.toString());
        JSONObject buildResult = new JSONObject(jsonObject);
        botBuilder.notifyBuildCompletionAndUnlock(buildResult);
        return "SUCCESS";
    }

    @RequestMapping(method= RequestMethod.POST, value="/rpa/create",  produces=MediaType.TEXT_PLAIN_VALUE)
    public String createBot(@RequestBody BasicBot bot) {
        JenkinsServer jenkinsServer = RpaJenkinsClientService.jenkinsServer;
        PythonBot pythonBot = (PythonBot) bot;
        String botName = pythonBot.getBotName();
        try {
            JobWithDetails jobDetails = jenkinsServer.getJob(bot.getBotName());
            if(null!=jobDetails) {
                return "BOT_ALREADY_EXISTS";
            }
        } catch (IOException e) {
            log.error("Error while fetching job details",e);
        }
        String botXml = botCreationUtils.createBot(pythonBot);
        try {
            jenkinsServer.createJob(botName,botXml,true);
        } catch (IOException e) {
            log.error("Error while creating bot",e);
        }
        return "SUCCESS";
    }

    @RequestMapping(method= RequestMethod.GET, value="/rpa/step",  produces=MediaType.TEXT_PLAIN_VALUE)
    public String stepDescriptor() {
        BotCreationUtils botCreationUtils = new BotCreationUtils();
        botCreationUtils.generatePipelineScript(null, null, null);
        return "SUCCESS";
    }

    @RequestMapping(method= RequestMethod.GET, value="/rpa/trigger",  produces="application/json")
    public Map<String,Object> triggerJob(@RequestParam("botName") String botName) throws IOException {
        Map<String,Object> jsonObject = new HashMap<>();
        String requestId = UUID.randomUUID().toString();
        try {
            Map<String,String> requestParams = new HashMap<>();
            requestParams.put("scriptName", "/tmp/jdrpa/rest.py");
            requestParams.put("requestId",requestId);
            requestParams.put("token","welcome");

            log.info("Received Request for build and request Id: {}",requestId);

            Map<String,Object> result = botBuilder.triggerJobSync(botName, requestParams, requestId);
            log.info("Request completed for request Id: {}",requestId);
            return result;

        } catch (Exception e) {
            log.error("Error in building job for request Id {}",requestId,e);
            jsonObject.put("requestId",requestId);
            jsonObject.put("status", "failed");
            return jsonObject;
        }
    }

    @RequestMapping(method= RequestMethod.GET, value="/rpa/trigger/async/parallel",  produces="application/json")
    public String triggerParallelJobs() {
        Thread t1 = new Thread(new ParallelUtils(rpaJenkinsClientService,"Pipeline_1"));
        Thread t2 = new Thread(new ParallelUtils(rpaJenkinsClientService,"Pipeline_2"));
        Thread t3 = new Thread(new ParallelUtils(rpaJenkinsClientService,"Pipeline_3"));
        t1.start();
        t2.start();
        t3.start();
        return "OK";
    }

    @RequestMapping(method= RequestMethod.GET, value="/rpa/trigger/async",  produces="application/json")
    public Map<String,String> triggerJobAsync(@RequestParam("bot_name") String bot_name) throws IOException {

        Map<String,String> resp = new HashMap<>();
        String requestId = UUID.randomUUID().toString();

        try {
            Map<String,String> requestParams = new HashMap<>();
                requestParams.put("scriptName", "/tmp/jdrpa/rest.py");
            requestParams.put("requestId",requestId);
            requestParams.put("token","welcome");
            log.info("Received Request for build: "+i++);

            String queue = rpaJenkinsClientService.trigger(bot_name, requestParams,requestId);
            resp.put("request_no", String.valueOf(i));
            resp.put("queueId", queue);


        } catch (Exception e) {
            log.error("Error in building job",e);
        }
        return resp;
    }

}
