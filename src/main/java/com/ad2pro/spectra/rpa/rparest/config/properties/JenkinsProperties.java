package com.ad2pro.spectra.rpa.rparest.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="com.ad2pro.spectra.rpa.jenkins")
@Data
public class JenkinsProperties {
    private String url;
    private String username;
    private String password;
    private String token;
}
