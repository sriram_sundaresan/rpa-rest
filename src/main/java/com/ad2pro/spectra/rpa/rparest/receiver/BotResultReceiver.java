package com.ad2pro.spectra.rpa.rparest.receiver;

import com.ad2pro.spectra.common.codec.TypedJsonJacksonCodec;
import com.ad2pro.spectra.common.consumer.CommonConsumer;
import com.ad2pro.spectra.common.exception.MessageConsumerException;
import com.ad2pro.spectra.common.pojo.BuildResult;
import com.ad2pro.spectra.rpa.rparest.builder.BotBuilder;
import com.ad2pro.spectra.rpa.rparest.service.RpaJenkinsClientService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Component
public class BotResultReceiver {

    //TODO - optimise all these
    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    @Autowired
    @Qualifier("RedissonConsumer")
    private CommonConsumer commonConsumer;

    @Autowired
    private BotBuilder botBuilder;

    @Autowired
    RpaJenkinsClientService jenkinsClientService;

    @EventListener(ApplicationReadyEvent.class)
    public void startConsumerExecutor() {
        try {
            jenkinsClientService.init();
        } catch (URISyntaxException e) {
            log.error("Error while initiating Jenkins",e);
        }
        while (true) {
            ObjectMapper objectMapper = new ObjectMapper();
            //TODO - queueName is hardcoded for now. No entries found in the Delta Config table, returning the version as 0
            Map<String,Object> event = new LinkedHashMap();
            try {
                event = commonConsumer.readMessage("rpaMessage",new TypedJsonJacksonCodec(LinkedHashMap.class, byte[].class));
                log.info("Read {} from consumer. Converting object to RedisEvent.", event);
                BuildResult buildResult = objectMapper.convertValue(event, BuildResult.class);
                //org.json.JSONObject jsonObject = objectMapper.convertValue(objectMapper.writeValueAsString(buildResult),  org.json.JSONObject.class );
                executorService.execute(new BotResultWorkerThread(botBuilder, event));
            } catch (MessageConsumerException e) {
                log.error("Errors occurred while reading message from queue rpaMessage", e);
            }
        }
    }
}

