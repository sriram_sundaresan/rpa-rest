package com.ad2pro.spectra.rpa.rparest.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BotScriptLocation {
    @JsonProperty("locationType")
    private LocationType locationType;
    @JsonProperty("location")
    private String location;
    @JsonProperty("scmUrl")
    private String scmUrl;
    @JsonProperty("credential")
    private String credential;
    @JsonProperty("ftpServer")
    private String ftpServer;
    @JsonProperty("ftpServerLocation")
    private String ftpServerLocation;
}
